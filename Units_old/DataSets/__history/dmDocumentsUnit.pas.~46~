unit dmDocumentsUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  RESTClientModuleUnit, Data.FireDACJSONReflect, DllTypesUnit
  ,System.IOUtils, FireDAC.Stan.StorageBin
  ;

type
  TdmDocuments = class(TDataModule)
    FDMemDocumentList: TFDMemTable;
    FDMemDocumentListDocID: TIntegerField;
    FDMemDocumentListDescription: TStringField;
    FDMemDocumentListDATAAREAID: TSmallintField;
    FDMemDocVersions: TFDMemTable;
    FDMemDocVersionsDOCID: TIntegerField;
    FDMemDocVersionsVERSIONID: TIntegerField;
    FDMemDocVersionsFILEPATH: TStringField;
    FDMemDocVersionsFILENAME: TStringField;
    FDMemDocVersionsDATAAREAID: TSmallintField;
    FDMemDocVersionsCREATEDBY: TStringField;
    FDMemDocVersionsCREATEDDATE: TSQLTimeStampField;
    FDMemDocument: TFDMemTable;
    FDMemDocumentDOCID: TIntegerField;
    FDMemDocumentFILENAME: TStringField;
    FDMemDocumentDOCUMENT: TBlobField;
    FDMemDocumentDATAAREAID: TSmallintField;
    FDMemDocumentRELTABLE: TIntegerField;
    FDMemDocumentRELTABLELINEID: TIntegerField;
    FDMemDocumentEMPLID: TStringField;
    FDMemDocumentDOCVERSIONID: TIntegerField;
    FDMemGlobalDocList: TFDMemTable;
    FDMemGlobalDocListDOCID: TIntegerField;
    FDMemGlobalDocListDESCRIPTION: TStringField;
    FDMemGlobalDocListDATAAREAID: TSmallintField;
    FDMemGlobalDocListRELTABLEID: TSmallintField;
    FDMemGlobalDocListRELTABLELINEID: TIntegerField;
    FDMemGlobalDocListTABLENAME: TStringField;
    FDMemGlobalDocListMASTERFORMNAME: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDMemDocumentListAfterScroll(DataSet: TDataSet);
  private
    Doc: TDocRecord;
    RestClient: TClientModule1;
    ClientFilePath: string;
    FFileNameToSave: string;
    procedure GetDocumentVersion;
    procedure SetFileNameAndPath(FileName: string);
  public
    RefCount: integer;
    procedure GetDocumentList;
    procedure GetGlobalDocList;
    procedure ShowDocument;
    procedure SaveDocument(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string);
    property DocPathAndFileName: string read FFileNameToSave write SetFileNameAndPath;
  end;

var
  dmDocuments: TdmDocuments;

implementation

uses
  FMX.Dialogs
  , System.IniFiles
  , Winapi.ShellAPI
  , Winapi.Windows
  ;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

const
  GetDoc = 'GETDOC';
  sDocument = 'DOCUMENT';



procedure TdmDocuments.DataModuleCreate(Sender: TObject);
var
    IniFile: TIniFile;
begin
    RefCount:=0;
    IniFile:=TIniFile.Create('DocClient.ini');
    try
        ClientFilePath:= IniFile.ReadString('Path','ClientFilePath','');
        RestClient:=TClientModule1.Create(nil);
    finally
        IniFile.Free;
    end;
end;

procedure TdmDocuments.DataModuleDestroy(Sender: TObject);
begin
    RestClient.Free;
    dmDocuments:=nil;
end;

procedure TdmDocuments.FDMemDocumentListAfterScroll(DataSet: TDataSet);
begin
    //showmessage(FDMemDocumentListDocID.AsString);
    GetDocumentVersion;
end;

procedure TdmDocuments.GetDocumentList;
var
  LDataSetList: TFDJSONDataSets;
begin
    FDMemDocumentList.Close;
    LDataSetList:=RestClient.ServerMethods1Client.DocumentList;
    FDMemDocumentList.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));
    FDMemDocumentList.Open;
end;


procedure TdmDocuments.GetDocumentVersion;
var
  LDataSetList: TFDJSONDataSets;
begin
    if (FDMemDocumentList.Active) and not (FDMemDocumentListDocID.IsNull) then
    begin
        FDMemDocVersions.Close;
        LDataSetList:=RestClient.ServerMethods1Client.GetDocVersion(FDMemDocumentListDocID.Value);
        FDMemDocVersions.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));
        FDMemDocVersions.Open;
    end;
    //else
    //    Showmessage('No document selected');
end;


procedure TdmDocuments.GetGlobalDocList;
var
  LDataSetList: TFDJSONDataSets;
begin

    FDMemGlobalDocList.Close;
    LDataSetList:=RestClient.ServerMethods1Client.GlobalDocList;
    //showmessage(LDataSetList.ToString);
    FDMemGlobalDocList.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));
    FDMemGlobalDocList.Open;
end;

procedure TdmDocuments.SaveDocument(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string);
var
  fn: string;
  fs, fsCopy: TFileStream;
  JSONDataSet: TFDJSONDataSets;

begin
   if length(FFileNameToSave) < 6 then
      Exit;

   fn:=Doc.FILEPATH; //+Doc.FILENAME;

   if not FDMemDocument.Active then
      FDMemDocument.Open;

   FDMemDocument.EmptyDataSet;

   if FileExists(fn) then
       fs := TFileStream.Create(fn, fmOpenRead )
   else
       exit;

   Doc.DOCID:=-1;
   Doc.DATAAREAID:=DataAreaID;
   Doc.REFTABLE:=RefTable;
   Doc.REFTABLELINEID:=RefTableLineID;
   Doc.DOCVERSIONID:=1;
   Doc.EMPLID:=EmplID;

   try
      FDMemDocument.Insert;
      FDMemDocumentDOCID.Value:=Doc.DOCID;
      FDMemDocumentDATAAREAID.Value:=Doc.DATAAREAID;
      FDMemDocumentRELTABLE.Value:=Doc.REFTABLE;
      FDMemDocumentRELTABLELINEID.Value:=Doc.REFTABLELINEID;
      FDMemDocumentFILENAME.Value:=Doc.FileName;
      FDMemDocumentDOCVERSIONID.Value:=Doc.DOCVERSIONID;
      FDMemDocumentDOCUMENT.LoadFromStream(fs);
      FDMemDocumentEMPLID.Value:=Doc.EMPLID;
      FDMemDocument.Post;

      JSONDataSet := TFDJSONDataSets.Create;
      TFDJSONDataSetsWriter.ListAdd(JSONDataSet, sDocument, FDMemDocument);
      RestClient.ServerMethods1Client.SaveDoc(JSONDataSet);
   finally
      fs.Free;
   end;

   showmessage(FDMemDocumentDOCID.AsString);
end;

procedure TdmDocuments.SetFileNameAndPath(FileName: string);
begin
    FFileNameToSave:=FileName;
    Doc.FileName:=TPath.GetFileName(dmDocuments.FFileNameToSave);
    Doc.FILEPATH:=dmDocuments.FFileNameToSave;
end;

procedure TdmDocuments.ShowDocument;
var
  LDataSetDocument: TFDJSONDataSets;
  fs: TFileStream;
  fn: string;
  Doc: TDocRecord;
begin
    FDMemDocument.Open;
    LDataSetDocument:=RestClient.ServerMethods1Client.GetDocument(FDMemDocVersionsDOCID.Value, FDMemDocVersionsVERSIONID.Value);
    FDMemDocument.EmptyDataSet;
    FDMemDocument.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetDocument,0));

    Doc.FILEPATH:=ClientFilePath;
    Doc.FILENAME:=FDMemDocumentFILENAME.AsString;
    fn:=ClientFilePath+FDMemDocumentFILENAME.AsString;
    //EditSaveFile.Text:=FDMemDocumentFILENAME.AsString;

    if FileExists(fn) then
       fs := TFileStream.Create(fn, fmOpenWrite )
    else
       fs := TFileStream.Create(fn, fmCreate);

    try
        FDMemDocumentDOCUMENT.SaveToStream(fs);
        if (fs.Size > 0) then
        begin
            ShellExecute(0,'open',pchar(fn),nil,nil,SW_SHOWNORMAL);
        end
        else
            Showmessage('File is empty.');
    finally
        fs.Free;
    end;
end;

{procedure TTabbedwithNavigationForm.OpenDocument;
var
  LDataSetDocument: TFDJSONDataSets;
  fs: TFileStream;
  fn: string;
  RestClient: TClientModule1;
begin
    RestClient:=TClientModule1.Create(nil);
    try
        if FDMemDocumentList.Active = false then
        begin
            Showmessage('Get documentlist first.');
            Exit;
        end
        else if FDMemDocVersions.Active = False then
        begin
            Showmessage('Get documenversion first.');
            Exit;
        end;

        FDMemDocument.Open;
        LDataSetDocument:=RestClient.ServerMethods1Client.GetDocument(FDMemDocVersionsDOCID.Value, FDMemDocVersionsVERSIONID.Value);
        FDMemDocument.EmptyDataSet;
        FDMemDocument.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetDocument,0));
        //FDMemDocument.Open;
        //showmessage(FDMemDocumentDOCID.AsString);

        Doc.FILEPATH:=ClientFilePath;
        Doc.FILENAME:=FDMemDocumentFILENAME.AsString;
        fn:=ClientFilePath+FDMemDocumentFILENAME.AsString;
        EditSaveFile.Text:=FDMemDocumentFILENAME.AsString;

        if FileExists(fn) then
           fs := TFileStream.Create(fn, fmOpenWrite )
        else
           fs := TFileStream.Create(fn, fmCreate);

        try
            FDMemDocumentDOCUMENT.SaveToStream(fs);
            if (fs.Size > 0) then
            begin
                ShellExecute(0,'open',pchar(fn),nil,nil,SW_SHOWNORMAL);
            end
            else
                Showmessage('File is empty.');
        finally
            fs.Free;

            if FileExists(fn) then
              fs := TFileStream.Create(fn, fmOpenRead or fmShareDenyWrite);
            try
                MemoHash.Lines.Add('Open: '+ MD5(fs));
            finally
                fs.Free;
            end;
        end;
    finally
        RestClient.Free;
    end;
end; }






end.

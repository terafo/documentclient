object dmMaster: TdmMaster
  OldCreateOrder = False
  Height = 401
  Width = 510
  object FDMemDocumentList: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'DocID'
        DataType = ftInteger
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 48
    Top = 33
    object FDMemDocumentListDocID: TIntegerField
      FieldName = 'DocID'
    end
    object FDMemDocumentListDescription: TStringField
      FieldName = 'Description'
      Size = 100
    end
    object FDMemDocumentListDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
    end
  end
  object FDMemDocument: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'DOCID'
        DataType = ftInteger
      end
      item
        Name = 'FILENAME'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DOCUMENT'
        DataType = ftBlob
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
      end
      item
        Name = 'RELTABLE'
        DataType = ftInteger
      end
      item
        Name = 'RELTABLELINEID'
        DataType = ftInteger
      end
      item
        Name = 'EMPLID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DOCVERSIONID'
        DataType = ftInteger
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 48
    Top = 89
    object FDMemDocumentDOCID: TIntegerField
      FieldName = 'DOCID'
    end
    object FDMemDocumentFILENAME: TStringField
      FieldName = 'FILENAME'
      Size = 100
    end
    object FDMemDocumentDOCUMENT: TBlobField
      FieldName = 'DOCUMENT'
    end
    object FDMemDocumentDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
    end
    object FDMemDocumentRELTABLE: TIntegerField
      FieldName = 'RELTABLE'
    end
    object FDMemDocumentRELTABLELINEID: TIntegerField
      FieldName = 'RELTABLELINEID'
    end
    object FDMemDocumentEMPLID: TStringField
      FieldName = 'EMPLID'
      Size = 10
    end
    object FDMemDocumentDOCVERSIONID: TIntegerField
      FieldName = 'DOCVERSIONID'
    end
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 336
    Top = 144
  end
  object FDStanStorageXMLLink1: TFDStanStorageXMLLink
    Left = 336
    Top = 208
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 336
    Top = 272
  end
end

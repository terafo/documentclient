library DocClientDLL;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Sharemem,
  System.SysUtils,
  System.Classes,
  FMX.Dialogs,
  FireDAC.Comp.Client,
  Data.FireDACJSONReflect,
  dmMasterUnit in '..\DataSets\dmMasterUnit.pas' {dmMaster: TDataModule},
  RESTClientModuleUnit in '..\RESTClient\RESTClientModuleUnit.pas' {ClientModule1: TDataModule},
  RESTClientClassesUnit1 in '..\RESTClient\RESTClientClassesUnit1.pas',
  DllTypesUnit in 'DllTypesUnit.pas',
  FormOverviewUnit in '..\Forms\FormOverviewUnit.pas' {FormDocumentOverview},
  dmDocumentsUnit in '..\DataSets\dmDocumentsUnit.pas' {dmDocuments: TDataModule},
  FormSaveDocumentUnit in '..\Forms\FormSaveDocumentUnit.pas' {FormSaveDocument},
  FormBaseUnit in '..\Forms\FormBaseUnit.pas' {FormBase},
  FormGlobalDocListUnit in '..\Forms\FormGlobalDocListUnit.pas' {FormGlobalDocList};

{$R *.res}



procedure DllMessage; export;
begin
    Showmessage('Hello from dll.');
end;



function GetDocVersions(DocID:integer): TFDJSONDataSets; export;
var
    dmMaster: TdmMaster;
begin
    dmMaster:=TdmMaster.Create(nil);
    try
        result:=dmMaster.GetDocVersions(DocID);
    finally
        //dmMaster.Free;
    end;
end;


procedure SaveDocument(Doc: PDocRecord); export;
var
  dmMaster: TdmMaster;
begin
    dmMaster:=TdmMaster.Create(nil);
    try
        dmMaster.SaveDocument(Doc);
    finally
        dmMaster.Free;
    end;
end;

function GetDocument(DocID, DocVersion: Integer): TFDJSONDataSets; export;
var
  dmMaster: TdmMaster;
begin
    dmMaster:=TdmMaster.Create(nil);
    try
        dmMaster.GetDocument(DocID, DocVersion);
    finally
        dmMaster.Free;
    end;
end;

procedure SaveDocument2(FileName: string); export;
begin
    Showmessage(FileName);
end;

// Nyggjar funktionir

procedure OpenAllDocumentList(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string); export; stdcall;
var
    FormDocOverview: TFormDocumentOverview;
begin
    FormDocOverView:=TFormDocumentOverview.create(nil);
    FormDocOverView.RefTable:=RefTable;
    FormDocOverview.DataAreaID:=DataAreaID;
    FormDocOverView.REfTableLineID:=RefTableLineID;
    FormDocOverView.EmplID:=EmplID;
    FormDocOverView.ShowModal;
    FormDocOverView.Free;
end;

procedure OpenGlobalDocList; export;
var
    FormGlobalDoc: TFormGlobalDocList;
begin
    FormGlobalDoc:= TFormGlobalDocList.Create(nil);
    FormGlobalDoc.Show;
end;


procedure SaveDocumentForm(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string); export; stdcall;
//var
//   FormSaveDocument: TFormSaveDocument;
begin
   if not assigned(FormSaveDocument) then
      FormSaveDocument:=TFormSaveDocument.Create(nil);

   FormSaveDocument.EditSelectedFileName.Text:='';
   dmDocuments.DocPathAndFileName:='';
   FormSaveDocument.DataAreaID:=DataAreaID;
   FormSaveDocument.RefTable:=RefTable;
   FormSaveDocument.RefTableLineID:=RefTableLineID;
   FormSaveDocument.EmplID:=EmplID;
   FormSaveDocument.Show;
end;



exports
  DllMessage
  ,GetDocument     // Get selected document
  ,GetDocVersions   // Get selected doc version
  ,OpenAllDocumentList // Get All documents for selected relationID
  ,OpenGlobalDocList   // Get global documentlist
  ,SaveDocumentForm    // Open Savedocument form
  ;

begin
end.

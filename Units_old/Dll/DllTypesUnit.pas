unit DllTypesUnit;

interface

type
  PDocRecord = ^TDocRecord;
  TDocRecord = record
    DOCID: Integer;
    DATAAREAID: Smallint;
    REFTABLE: Integer;
    REFTABLELINEID: Integer;
    FILEPATH: string;
    FILENAME: String;
    DOCVERSIONID: Integer;
    //DOCUMENT: TFileStream;
    EMPLID: String;
  end;

implementation

end.

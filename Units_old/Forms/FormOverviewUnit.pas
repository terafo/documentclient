unit FormOverviewUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, System.Rtti, FMX.Grid.Style,
  FMX.ScrollBox, FMX.Grid, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope
  ,dmDocumentsUnit, FMX.TabControl, FormBaseUnit
  ;

type
  TFormDocumentOverview = class(TForm)
    Panel1: TPanel;
    btnShowDocument: TButton;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    StringGrid2: TStringGrid;
    StringGrid1: TStringGrid;
    BindingsList1: TBindingsList;
    BindSourceDB1: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btnShowDocumentClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    FDataAreaID: SmallInt;
    FRefTable: integer;
    FRefTableLineID: integer;
    FEmplID: string;
    procedure SetBasePanDataAreaID(DataAreaID: smallint);
    procedure SetBasePanRefTable(RefTable: integer);
    procedure SetBasePanRefTableLineID(RefTableLineId: integer);
    procedure SetBasePanEmplID(EmplID: string);
  public
    property DataAreaID: Smallint read FDataAreaID write SetBasePanDataAreaID;
    property RefTable: integer read FRefTable write SetBasePanRefTable;
    property RefTableLineID: integer read FRefTableLineID write SetBasePanRefTableLineID;
    property EmplID: string read FemplID write SetBasePanEmplID;
  end;




implementation

{$R *.fmx}

procedure TFormDocumentOverview.btnShowDocumentClick(Sender: TObject);
begin
    //dmDocuments.ShowDocument;
end;

procedure TFormDocumentOverview.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    {dmDocuments.RefCount:=dmDocuments.RefCount-1;

    if (dmDocuments.RefCount = 0) then
        dmDocuments.Free;
    }
    //Action:=TCloseAction.caFree;
end;

procedure TFormDocumentOverview.FormCreate(Sender: TObject);
begin
    inherited;

    {if not assigned(dmDocuments) then
    begin
       dmDocuments:=TdmDocuments.Create(nil);
       dmDocuments.RefCount:=dmDocuments.RefCount+1;
    end; }
end;

procedure TFormDocumentOverview.FormShow(Sender: TObject);
begin
  inherited;
    //dmDocuments.GetDocumentList(RefTable,DataAreaID,RefTableLineID,EmplID);
end;

procedure TFormDocumentOverview.SetBasePanDataAreaID(DataAreaID: smallint);
begin
    FDataAreaID:=DataAreaID;
    //LabelBaseDataAreaID.Text:=intToStr(FDataAreaID);
end;

procedure TFormDocumentOverview.SetBasePanEmplID(EmplID: string);
begin
    FEmplId:=EmplID;
    //LabelBaseEmplId.Text:=FEmplID;
end;

procedure TFormDocumentOverview.SetBasePanRefTable(RefTable: integer);
begin
    FRefTable:=RefTable;
    //LabelBaseRef.Text:=FRefTable.ToString+' - '+FRefTableLineID.ToString;
end;

procedure TFormDocumentOverview.SetBasePanRefTableLineID(
  RefTableLineId: integer);
begin
    FRefTableLineID:=RefTableLineID;
    //LabelBaseRef.Text:=FRefTable.ToString+' - '+FRefTableLineID.ToString;
end;

end.

unit FormGlobalDocListUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FormBaseUnit,
  dmDocumentsUnit, System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid,
  FMX.TabControl, FMX.StdCtrls, FMX.Controls.Presentation, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope;

type
  TFormGlobalDocList = class(TFormBase)
    Panel1: TPanel;
    Button1: TButton;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    StringGrid1: TStringGrid;
    BindingsList1: TBindingsList;
    BindSourceDB1: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormGlobalDocList: TFormGlobalDocList;

implementation

{$R *.fmx}

procedure TFormGlobalDocList.Button1Click(Sender: TObject);
begin
  inherited;
    dmDocuments.GetGlobalDocList;
end;

procedure TFormGlobalDocList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
     dmDocuments.RefCount:=dmDocuments.RefCount-1;

    if (dmDocuments.RefCount = 0) then
        dmDocuments.Free;

    Action:=TCloseAction.caFree;
end;

procedure TFormGlobalDocList.FormCreate(Sender: TObject);
begin
  inherited;
    if not assigned(dmDocuments) then
    begin
       dmDocuments:=TdmDocuments.Create(nil);
       dmDocuments.RefCount:=dmDocuments.RefCount+1;
    end;
end;

end.

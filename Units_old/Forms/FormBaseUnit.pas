unit FormBaseUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation;

type
  TFormBase = class(TForm)
    StatusBar1: TStatusBar;
    PanelBaseRef: TPanel;
    PanelBaseDataAreaID: TPanel;
    PanelBaseEmplID: TPanel;
    Panel4: TPanel;
    LabelBaseRef: TLabel;
    LabelBaseDataAreaID: TLabel;
    LabelBaseEmplId: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormBase: TFormBase;

implementation

{$R *.fmx}

end.

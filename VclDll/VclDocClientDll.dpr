library VclDocClientDll;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Sharemem,
  System.SysUtils,
  System.Classes,
  FormDocListByRefIDUnit in 'Units\Forms\FormDocListByRefIDUnit.pas' {FormDocListByRefID},
  FormDllBaseUnit in 'Units\Forms\FormDllBaseUnit.pas' {FormDllBase},
  DBGridFilterUnit in 'Units\DbGrid\DBGridFilterUnit.pas',
  GridFilterUnit in 'Units\DbGrid\GridFilterUnit.pas',
  dmDocumentsUnit in 'Units\DataModule\dmDocumentsUnit.pas' {dmDocuments: TDataModule},
  RESTClientModuleUnit in 'Units\REST\RESTClientModuleUnit.pas' {ClientModule1: TDataModule},
  DllTypesUnit in 'Units\Misc\DllTypesUnit.pas',
  FormNewDocUnit in 'Units\Forms\FormNewDocUnit.pas' {FormNewDocument},
  UtilUnit in 'Units\Misc\UtilUnit.pas',
  IObserverUnit in 'Units\Misc\IObserverUnit.pas' {/, RESTClientClassesUnit1 in 'Units\REST\RESTClientClassesUnit1.pas'},
  ClientClassesUnit1 in 'ClientClassesUnit1.pas';

{$R *.res}




procedure OpenDocListByRefID(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string); export; stdcall;
var
    Frm: TFormDocListByRefID;
begin
    Frm:=TFormDocListByRefID.Create(nil);
    try
        dmDocuments.REFTABLE:=RefTable;
        dmDocuments.REFTABLELINEID:=RefTableLineID;
        dmDocuments.DATAAREAID:=DataAreaID;
        dmDocuments.UserID:=EmplID;
        Frm.ShowModal;
    finally
        Frm.Free;
    end;
end;

procedure OpenGlobalDocList;
var
    Frm: TFormDocListByRefID;
begin
    Frm:=TFormDocListByRefID.Create(nil);
    try

        dmDocuments.REFTABLE:=-1;
        dmDocuments.REFTABLELINEID:=-1;
        dmDocuments.DATAAREAID:=1;
        dmDocuments.UserID:='999';


    finally
        Frm.Free;
    end;

end;

exports
    OpenDocListByRefID
    ,OpenGlobalDocList
    ;

end.

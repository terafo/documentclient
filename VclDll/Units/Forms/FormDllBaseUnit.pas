unit FormDllBaseUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ToolWin,
  System.ImageList, Vcl.ImgList, Vcl.ExtCtrls, DllTypesUnit, dmDocumentsUnit
  //, IObserverUnit
  ;

type
  TFormDllBase = class(TForm)
    StatusBareBase: TStatusBar;
    ImageListBase: TImageList;
    ControlBar1: TControlBar;
    ToolBarBase: TToolBar;
    tBtnBaseRefresh: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    procedure SetBasePanDataAreaID;
    procedure SetBasePanRefTable;
    procedure SetBasePanRefTableLineID;
    procedure SetBasePanEmplID;
    //procedure SetFileNameAndPath(FileName: string);
  public
    //property DataAreaID: Smallint read dmDocuments.Doc.DataAreaID write SetBasePanDataAreaID;
    //property RefTable: integer read dmDocuments.Doc.RefTable write SetBasePanRefTable;
    //property RefTableLineID: integer read dmDocuments.Doc.RefTableLineID write SetBasePanRefTableLineID;
    //property EmplID: string read dmDocuments.Doc.EmplID write SetBasePanEmplID;
    //property FileNameAndPath: string write SetFileNameAndPath;
    //property FileName: string read dmDocuments.Doc.fileName;
    //property FilePath: string read dmDocuments.Doc.FilePath;
    //
  end;

var
  FormDllBase: TFormDllBase;

implementation

uses
  System.IOUtils;

{$R *.dfm}

procedure TFormDllBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin

    dmDocuments.RefCount:=dmDocuments.RefCount-1;

    if (dmDocuments.RefCount < 1) then
       dmDocuments.Free;

    Action:=caFree;
end;

procedure TFormDllBase.FormCreate(Sender: TObject);
begin
    if not Assigned(dmDocuments) then
    begin
       dmDocuments:=TdmDocuments.Create(self);
       dmDocuments.RefCount:=1;
    end
    else
       dmDocuments.RefCount:=dmDocuments.RefCount+1;


    StatusBareBase.Panels[4].Text:=dmDocuments.ClientFilePath;
end;

procedure TFormDllBase.FormShow(Sender: TObject);
begin
    SetBasePanDataAreaID;
    SetBasePanRefTable;
    SetBasePanRefTableLineID;
    SetBasePanEmplID;
end;

procedure TFormDllBase.SetBasePanDataAreaID;
begin
   //dmDocuments.DataAreaID:=DataAreaID;
   StatusBareBase.Panels[0].Text:=intToSTr(dmDocuments.DataAreaID);
end;

procedure TFormDllBase.SetBasePanEmplID;
begin
   //dmDocuments.EmplId:=EmplID;
   StatusBareBase.Panels[1].Text:=dmDocuments.EmplID;
end;

procedure TFormDllBase.SetBasePanRefTable;
begin
   //dmDocuments.RefTable:=RefTable;
   StatusBareBase.Panels[2].Text:=dmDocuments.RefTable.ToString;
end;

procedure TFormDllBase.SetBasePanRefTableLineID;
begin
    //dmDocuments.RefTableLineID:=RefTableLineID;
    StatusBareBase.Panels[3].Text:=dmDocuments.RefTableLineID.ToString;
end;

{procedure TFormDllBase.SetFileNameAndPath(FileName: string);
begin
    dmDocuments.Doc.FileNameAndPath:=FileName;
end;  }



end.

unit dmDocumentsUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  RESTClientModuleUnit, Data.FireDACJSONReflect, DllTypesUnit
  ,System.IOUtils, FireDAC.Stan.StorageBin, UtilUnit, Vcl.ExtCtrls,
  IObserverUnit
  ,System.Generics.Collections
  ;

type
  TdmDocuments = class(TDataModule, ISubject)
    FDMemDocVersions: TFDMemTable;
    FDMemDocVersionsDOCID: TIntegerField;
    FDMemDocVersionsVERSIONID: TIntegerField;
    FDMemDocVersionsFILEPATH: TStringField;
    FDMemDocVersionsFILENAME: TStringField;
    FDMemDocVersionsDATAAREAID: TSmallintField;
    FDMemDocVersionsCREATEDBY: TStringField;
    FDMemDocVersionsCREATEDDATE: TSQLTimeStampField;
    FDMemDocument: TFDMemTable;
    FDMemDocumentDOCID: TIntegerField;
    FDMemDocumentFILENAME: TStringField;
    FDMemDocumentDOCUMENT: TBlobField;
    FDMemDocumentDATAAREAID: TSmallintField;
    FDMemDocumentRELTABLE: TIntegerField;
    FDMemDocumentRELTABLELINEID: TIntegerField;
    FDMemDocumentEMPLID: TStringField;
    FDMemDocumentDOCVERSIONID: TIntegerField;
    FDMemGlobalDocList: TFDMemTable;
    FDMemGlobalDocListDOCID: TIntegerField;
    FDMemGlobalDocListDESCRIPTION: TStringField;
    FDMemGlobalDocListDATAAREAID: TSmallintField;
    FDMemGlobalDocListRELTABLEID: TSmallintField;
    FDMemGlobalDocListRELTABLELINEID: TIntegerField;
    FDMemGlobalDocListTABLENAME: TStringField;
    FDMemGlobalDocListMASTERFORMNAME: TStringField;
    Timer1: TTimer;
    FDMemDocumentDESCRIPTION: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDMemGlobalDocListAfterScroll(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure FDMemGlobalDocListBeforeEdit(DataSet: TDataSet);
  private
    RestClient: TClientModule1;
    FClientFilePath: string;
    Doc: TDocRecord;
    procedure GetDocumentVersion;
    procedure SetFileNameAndPath(SelectedFileName: string);
    procedure SetFileDescription(NewDescription: string);
    function GetFileNameAndPath: string;
    function GetFileName: string;
    function GetFilePath: string;
  public
    FObserverList: TList<IFormObserver>;
    RefCount: integer;
    MD5OrgFile: string;
    MD5CurrentFile: string;
    UserID: string;
    FileChanged: Boolean;
    procedure RegisterObserver(aObserver: IFormOBserver);
    procedure RemoveObserver(aObserver: IFormOBserver);
    procedure NotifyObserver;
    procedure GetDocumentList(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string);
    procedure GetGlobalDocList;
    procedure ShowDocument;
    function SaveDocument: boolean;
    procedure UpdateDescription;
    procedure RefreshDocumentList;

    property DataAreaID: SmallInt read Doc.DataAreaID write Doc.DataAreaID;
    property EmplID: string read Doc.EmplID write Doc.EmplID;
    property RefTable: integer read Doc.RefTable write Doc.RefTable;
    property RefTableLineID: integer read Doc.RefTableLineID write Doc.ReftableLineID;
    property ClientFilePath: string read FClientFilePath;

    Property SelectedDoc: TDocRecord read Doc;
    property FileNameAndPathToSave: string read GetFileNameAndPath write SetFileNameAndPath;
    property FileName: string read GetFileName;
    property FilePath: string read GetFilePath;
    property FileDecription: string write SetFileDescription;
  end;

var
  dmDocuments: TdmDocuments;

implementation

uses
  Dialogs
  , System.IniFiles
  , Winapi.ShellAPI
  , Winapi.Windows
  ;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

const
  GetDoc = 'GETDOC';
  sDocument = 'DOCUMENT';



procedure TdmDocuments.DataModuleCreate(Sender: TObject);
var
    //IniFile: TIniFile;
    IniFile: TStringList;
begin
    RefCount:=0;
    FObserverList:= TList<IFormObserver>.Create;
    FileChanged:=False;

    //IniFile:=TIniFile.Create('Projectstyringx.ini');
    IniFile:=TStringList.Create;
    try
        IniFile.LoadFromFile('Projectstyring.ini');
        FClientFilePath:= IniFile.Values['ClientFilePath'];
        //FClientFilePath:= IniFile.ReadString('DocumentClient','ClientFilePath','');
        RestClient:=TClientModule1.Create(nil);
    finally
        IniFile.Free;
    end;
end;

procedure TdmDocuments.DataModuleDestroy(Sender: TObject);
begin
    RestClient.Free;
    //dmDocuments:=nil;
end;

procedure TdmDocuments.FDMemGlobalDocListAfterScroll(DataSet: TDataSet);
begin
    GetDocumentVersion;
end;

procedure TdmDocuments.FDMemGlobalDocListBeforeEdit(DataSet: TDataSet);
begin
    Doc.Description:=FDMemGlobalDocListDESCRIPTION.Value;
end;

procedure TdmDocuments.GetDocumentList(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string);
var
  LDataSetList: TFDJSONDataSets;
begin
    FDMemGlobalDocList.Close;
    LDataSetList:=RestClient.ServerMethods1Client.DocumentList(RefTable,DataAreaID,RefTableLineID,EmplID,-1);
   //showmessage(LDataSetList.TItemList);
    FDMemGlobalDocList.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));
    FDMemGlobalDocList.Open;
end;


procedure TdmDocuments.GetDocumentVersion;
var
  LDataSetList: TFDJSONDataSets;
begin
    if (FDMemGlobalDocList.Active) and not (FDMemGlobalDocListDocID.IsNull) then
    begin
        FDMemDocVersions.Close;
        LDataSetList:=RestClient.ServerMethods1Client.GetDocVersion(FDMemGlobalDocListDocID.Value);
        FDMemDocVersions.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));
        FDMemDocVersions.Open;
    end;
    //else
    //    Showmessage('No document selected');
end;


function TdmDocuments.GetFileName: string;
begin
    result:=Doc.FileName;
end;

function TdmDocuments.GetFileNameAndPath: string;
begin
    result:=Doc.FileNameAndPath
end;

function TdmDocuments.GetFilePath: string;
begin
    result:=Doc.FILEPATH;
end;

procedure TdmDocuments.GetGlobalDocList;
var
  LDataSetList: TFDJSONDataSets;
begin

    FDMemGlobalDocList.Close;
    LDataSetList:=RestClient.ServerMethods1Client.GlobalDocList;
    //showmessage(LDataSetList.ToString);
    FDMemGlobalDocList.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));
    FDMemGlobalDocList.Open;
end;

procedure TdmDocuments.NotifyObserver;
var
  FObserver: IFormObserver;
begin
    for FObserver in FObserverList do
  begin
    FObserver.UpdateForm(self);
    //FObserver.UpdateProgressBar(BaseProgBarPos);
  end;
end;

procedure TdmDocuments.RefreshDocumentList;
begin
    if (RefTable = -1) then
   begin
      GetGlobalDocList;
      GetDocumentVersion;
   end
   else
   begin
      GetDocumentList(RefTable,DataAreaID,RefTableLineID,EmplID);
      GetDocumentVersion;
   end;
end;

procedure TdmDocuments.RegisterObserver(aObserver: IFormOBserver);
begin
    FObserverList.Add(aObserver);
end;

procedure TdmDocuments.RemoveObserver(aObserver: IFormOBserver);
begin
    FObserverList.Remove(aOBserver);
end;

//procedure TdmDocuments.SaveDocument(RefTable, DataAreaID: smallint; RefTableLineID: integer; EmplID: string);
function TdmDocuments.SaveDocument: boolean;
var
  fn: string;
  fs: TFileStream;
  JSONDataSet: TFDJSONDataSets;
begin
   result:=false;

   if (Doc.REFTABLE < 1) then
      Exit;

   //if length(FFileNameToSave) < 6 then
   //   Exit;

   fn:=Doc.FileNameAndPath; //+Doc.FILENAME;

   if not FDMemDocument.Active then
      FDMemDocument.Open;

   FDMemDocument.EmptyDataSet;

   if FileExists(fn) then
       fs := TFileStream.Create(fn, fmOpenRead )
   else
       exit;

   try
      FDMemDocument.Insert;
      FDMemDocumentDOCID.Value:=Doc.DOCID;
      FDMemDocumentDATAAREAID.Value:=Doc.DATAAREAID;
      FDMemDocumentRELTABLE.Value:=Doc.REFTABLE;
      FDMemDocumentRELTABLELINEID.Value:=Doc.REFTABLELINEID;
      FDMemDocumentFILENAME.Value:=Doc.FileName;
      FDMemDocumentDESCRIPTION.Value:=Doc.Description;
      FDMemDocumentDOCVERSIONID.Value:=Doc.DOCVERSIONID;  //inDoc.DOCVERSIONID;
      FDMemDocumentDOCUMENT.LoadFromStream(fs);
      FDMemDocumentEMPLID.Value:=UserID;
      FDMemDocument.Post;

      JSONDataSet := TFDJSONDataSets.Create;
      TFDJSONDataSetsWriter.ListAdd(JSONDataSet, sDocument, FDMemDocument);
      RestClient.ServerMethods1Client.SaveDoc(JSONDataSet);
      FileChanged:=False;
      result:=True;

      RefreshDocumentList;
   finally
      fs.Free;
   end;
end;

procedure TdmDocuments.SetFileDescription(NewDescription: string);
begin
    Doc.Description:=NewDescription;
end;

procedure TdmDocuments.SetFileNameAndPath(SelectedFileName: string);
begin
    dmDocuments.Doc.FileNameAndPath:=SelectedFileName;
    NotifyObserver;
end;



procedure TdmDocuments.ShowDocument;
var
  LDataSetDocument: TFDJSONDataSets;
  fs: TFileStream;
  fn: string;
begin
    FileChanged:=False;
    FDMemDocument.Open;
    LDataSetDocument:=RestClient.ServerMethods1Client.GetDocument(FDMemDocVersionsDOCID.Value, FDMemDocVersionsVERSIONID.Value);
    FDMemDocument.EmptyDataSet;
    FDMemDocument.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetDocument,0));

    Doc.FileNameAndPath:=IncludeTrailingPathDelimiter(ClientFilePath)+FDMemDocumentFILENAME.AsString;
    Doc.DOCVERSIONID:=FDMemDocumentDOCVERSIONID.Value;
    Doc.DOCID:=FDMemDocVersionsDOCID.Value;
    Doc.DATAAREAID:=FDMemGlobalDocListDATAAREAID.Value;   //FDMemDocumentDATAAREAID.Value;
    Doc.REFTABLE:=FDMemGlobalDocListRELTABLEID.Value;
    Doc.REFTABLELINEID:=FDMemGlobalDocListRELTABLELINEID.Value;
    Doc.EMPLID:=FDMemDocVersionsCREATEDBY.Value;

    fn:=Doc.FileNameAndPath;   //IncludeTrailingPathDelimiter(ClientFilePath)+FDMemDocumentFILENAME.AsString;
    //FFileNameToSave:=FDMemDocumentFILENAME.AsString;

    if FileExists(fn) then
       DeleteFile(PCHar(fn));


    fs := TFileStream.Create(fn, fmCreate);
    try
        // Save document to client folder
        FDMemDocumentDOCUMENT.SaveToStream(fs);

        if (fs.Size = 0) then
        begin
            Showmessage('File is empty.');
            exit;
        end;
    finally
        fs.Free;
    end;

    fs := TFileStream.Create(fn, fmShareDenyNone);
    try
        MD5OrgFile:=MD5(fs);
    finally
        fs.Free;
    end;

    if FileExists(fn) then
       ShellExecute(0,'open',pchar(fn),nil,nil,SW_SHOWNORMAL);

    Timer1.Enabled:=True;
end;

procedure TdmDocuments.Timer1Timer(Sender: TObject);
var
    fs: TFileStream;
    fn: String;
begin
    fn:=IncludeTrailingPathDelimiter(ClientFilePath)+FDMemDocumentFILENAME.AsString;

    fs := TFileStream.Create(fn, fmShareDenyNone);
    try
        MD5CurrentFile:=MD5(fs);
        if MD5CurrentFile <> MD5OrgFile then
        begin
           Timer1.Enabled:=False;
           FileChanged:=True;
           Doc.DOCVERSIONID:=Doc.DOCVERSIONID+1;
           NotifyObserver;
        end;
    finally
        fs.Free;
    end;
end;

procedure TdmDocuments.UpdateDescription;
var
    JSONDataSet: TFDJSONDataSets;
begin
     FDMemDocument.Edit;
     FDMemDocumentDOCID.Value:=FDMemGlobalDocListDOCID.Value;
     //FDMemDocumentDATAAREAID.Value:=Doc.DATAAREAID;
     //FDMemDocumentRELTABLE.Value:=Doc.REFTABLE;
     //FDMemDocumentRELTABLELINEID.Value:=Doc.REFTABLELINEID;
     //FDMemDocumentFILENAME.Value:=Doc.FileName;
     FDMemDocumentDESCRIPTION.Value:=FDMemGlobalDocListDESCRIPTION.Value;
     //FDMemDocumentDOCVERSIONID.Value:=Doc.DOCVERSIONID;  //inDoc.DOCVERSIONID;
     //FDMemDocumentDOCUMENT.LoadFromStream(fs);
     //FDMemDocumentEMPLID.Value:=UserID;
     FDMemDocument.Post;

     JSONDataSet := TFDJSONDataSets.Create;
     TFDJSONDataSetsWriter.ListAdd(JSONDataSet, sDocument, FDMemDocument);
     RestClient.ServerMethods1Client.UpdateDocDescription(JSONDataSet);
end;

end.

unit IObserverUnit;

interface

type
   //TJobStatus = (jsRunning, jsFinished);

  IFormObserver = interface
  ['{1E6D1D21-BA22-4E34-B68B-BEBF3D5461E6}']
    procedure UpdateForm(Sender: TObject);
    //procedure UpdateProgressBar(iValue: integer);
  end;

  ISubject = interface
  ['{2A142094-87B9-43BB-B2B8-06F2A8308011}']
    procedure RegisterObserver(aObserver: IFormOBserver);
    procedure RemoveObserver(aObserver: IFormOBserver);
    procedure NotifyObserver;
  end;

implementation

end.

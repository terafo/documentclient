unit DllTypesUnit;

interface

type
  PDocRecord = ^TDocRecord;
  TDocRecord = record
  private
    FFileName: String;
    procedure SetFileNameAndPath(SelectedFileName: string);
    function GetFileNameAndPath: string;
  public
    DOCID: Integer;
    DATAAREAID: Smallint;
    REFTABLE: Integer;
    REFTABLELINEID: Integer;
    FILEPATH: string;
    DOCVERSIONID: Integer;
    EMPLID: String;
    Description: String;
    procedure ResetDoc;
    property FileNameAndPath: string read GetFileNameAndPath write SetFileNameAndPath;
    property FileName: string read FFileName;
  end;

implementation

uses
  System.IOUtils
  ,System.SysUtils
  ;

{ TDocRecord }

function TDocRecord.GetFileNameAndPath: string;
begin
    result:=IncludeTrailingPathDelimiter(FILEPATH)+FFileName;
end;

procedure TDocRecord.ResetDoc;
begin
    DOCID:=-1;
    DATAAREAID:= -1;
    REFTABLE:= -1;
    REFTABLELINEID:= -1;
    FILEPATH:='';
    FFileName:='';
    DOCVERSIONID:=-1;
    EMPLID:='';
end;

procedure TDocRecord.SetFileNameAndPath(SelectedFileName: string);
begin
    FFileName:=Tpath.GetFileName(SelectedFileName);
    FILEPATH:=TPath.GetDirectoryName(SelectedFileName); //GetFullPath(SelectedFileName);
    Description:=FFileName;
end;

end.

unit DBGridPopUpUnit;

interface

uses  Windows, Messages, DBGrids, Grids, System.Types, Classes , Graphics, GridFilterUnit,
      System.SysUtils,  DBClient, vcl.Controls, Data.DB, System.UITypes,
      vcl.ComCtrls, Vcl.Dialogs, Vcl.Forms, Vcl.Menus, System.Variants
      , TMyMenuItemUnit;


type
    IDBGridPopUpMenu = interface(IInterface)
      ['{8338D2B0-22F6-4C73-81DF-43A5BB6CD366}']
    end;

    
    

    TDbGridPopUpMenu = class(TPopupMenu,IDBGridPopUpMenu)
      procedure PopUp(X, Y: Integer); override;
    private
      FOnPopup: TNotifyEvent;
      procedure HandlePopupItemClick(Sender: TObject);
      procedure MyPopUp(Sender: TObject);
    public
      FGridTableName: string;
      FGridFieldName: string;
      FMasterTableRefID: integer;
      FLookUpValue: Variant;
      Constructor Create(AOwner: TComponent); override;
    published
      property OnPopup: TNotifyEvent read FOnPopup write FOnPopup;   
    end;




implementation

uses GoToMainFormUnit, DatamoduleUnit, TeraFunkUnit;


constructor TDbGridPopUpMenu.Create(AOwner: TComponent);
var
  Item: TMenuItem;
  i: Integer;
  FieldName: string;
begin
  inherited;
  self.AutoHotkeys := maManual;
  self.AutoPopup:=False;
  self.OwnerDraw:=False;
  self.OnPopup := MyPopUp;
  
  Item := TMenuItem.Create(self);
  Item.Caption := 'GoToMainForm';
  self.Items.Add(Item);
  Item.OnClick := HandlePopupItemClick;  
end;

procedure TDBGridPopUpMenu.PopUp(X, Y: Integer); 
begin
  inherited; 
end;

//procedure TDBGridPopUpMenu.PopUp(X, Y: Integer);
//var 
//   FGridTableName: string;
//   FGridFieldName: string;

//begin
  //inherited;
   //self.Items[0].Caption:=  (self.Owner).Name;
   {FGridTableName:=HeintaTeknSepareraFelt(TDBGrid(owner).SelectedField.Origin,'.','"',1);
   FGridFieldName:=HeintaTeknSepareraFelt(TDBGrid(owner).SelectedField.Origin,'.','"',2);
 
   with DataModule1 do
   begin
       if CdsTableFieldMasterTable.Locate('TableName;FieldName',VarArrayOf([FGridTableName,FGridFieldName]),[]) then
       begin
           self.Items[0].Enabled:=True;
       end
       else
          self.Items[0].Enabled:=False;
          //ShowMessage(CdsTableFieldMasterTableMainTable.Value+', '+CdsTableFieldMasterTableMainTableFieldName.Value);
       //if CdsTableFieldMasterTable.Locate('TableName;FieldName',varArrayOf([self.SelectedField.Origin,  self.SelectedField.FieldName then
   end;
   }
//   (owner as TForm).Refresh;
   
//end;



procedure TDBGridPopUpMenu.HandlePopupItemClick(Sender: TObject);
var
   FMainTableName: string;
   FMainTableFieldName: string;
   //LookUpTable_RefID: integer;
   //SenderTable_RefID: integer;
begin
   //ShowMessage(FGridTableName+' - '+FGridFieldName);

   with DataModule1 do
   begin
       if (FMasterTableRefID > 0) then
       begin
           TGoToMainForm.ShowMainForm(FMasterTableRefID, 0, FLookUpValue,'0');
       end
       else
       begin
           if CdsTableFieldMasterTable.Locate('TableName;FieldName',VarArrayOf([FGridTableName,FGridFieldName]),[]) then
           begin
              FMainTableName:=CdsTableFieldMasterTableMainTable.Value;
              FMainTableFieldName:=CdsTableFieldMasterTableMainTableFieldName.Value;

              //ShowMessage(FMainTableName+' - '+FMainTableFieldName);

              if CDSRefTables.Locate('TableName',FMainTableName,[]) then
              begin
                  TGoToMainForm.ShowMainForm(CDSRefTablesID.Value, 0, FLookUpValue,'0');
              end;
           end;
       end;
   end;




  //TGoToMainForm.ShowMainForm( LookUp_TableRefID, Sender_TableRefID: integer; LookUp_TableLineID,Sender_TableLineID: Variant);

  {with DataModule1 do
  begin
      Item := TMenuItem.Create(FPopup);
      Item.Caption := self.SelectedField.Origin;
      //Item.Caption := self.DataSource.DataSet.Field [self.SelectedIndex].Origin;
      FPopup.Items.Add(Item);
      //if CdsTableFieldMasterTable.Locate('TableName;FieldName',varArrayOf([TClientDataSet(self.DataSource.DataSet.Field  self.SelectedField.FieldName then
      Self.PopupMenu := FPopup;
  end;
  }
  {for i := 0 to 5 do
  begin
    Item := TMenuItem.Create(FPopup);
    Item.Caption := 'Item ' + IntToStr(i);
    Item.OnClick := HandlePopupItem;
    FPopup.Items.Add(Item);
  end;
   }
end;


procedure TDbGridPopUpMenu.MyPopUp(Sender: TObject);
begin
   //inherited PopUp(;
   self.Items[0].Caption:=  (self.Owner).Name;
end;

end.
